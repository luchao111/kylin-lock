package com.wjk.kylin.lock.example.custom;

import com.wjk.kylin.lock.fail.LockFailureCallBack;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 自定义默认加锁异常处理
 *
 * @author wangjinkui
 */
@Slf4j
@Component("lockFailureCallBack")
public class DefaultLockFailureCallBack implements LockFailureCallBack {

    @Override
    public void callBack(Method method, Object[] args) {
        log.error("{}, method:{}, args:{}", DEFAULT_MESSAGE, method, args);
        // 此处可以抛出指定异常，配合全局异常拦截包装统一格式返回给调用端
        throw new BusinessException(DEFAULT_MESSAGE);
    }
}
