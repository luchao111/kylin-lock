package com.wjk.kylin.lock.aop;

import org.aopalliance.intercept.MethodInterceptor;

/**
 * 增强接口
 *
 * @author wangjinkui
 */
public interface LockInterceptor extends MethodInterceptor {

}
